/* @flow */

import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import * as React from 'react';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';
import Error404 from './components/Error404';
import Dashboard from './components/Dashboard';

type Props = {};

type State = {};

export default class App extends React.Component<Props,State> {
	baseName: string;
	constructor(): void {
		super();
		this.state = {};

		this.baseName = '';
		let base_path = document.getElementById('base_path');
		if (base_path && typeof(base_path.value) == 'string') {
			this.baseName = base_path.value;
		}
	}

	componentDidMount() :void {
		var tout= null;
		if (!window.showPopup &&  document.getElementById('popup_alert') && document.getElementById('popup_alert_msg')) {
			window.showPopup = (type : string, message : string, timeout) => {
				if (!timeout) {timeout=5000;}
				var alert_msg = document.getElementById('popup_alert_msg');
				var popup_alert = document.getElementById('popup_alert');
				
				if(type != null && popup_alert){
					popup_alert.className = 'alert alert-'+type+' active';
				}
				if(message != null && alert_msg){
					alert_msg.innerHTML = message;
				}
				tout = setTimeout(() => {
					window.hidePopup();
				}, timeout);
				console.log('Message:',message);
			};
		}
		if (!window.hidePopup && document.getElementById('popup_alert')) {
			//let popup_alert = document.getElementById('popup_alert');
			window.hidePopup = () => {
				if(document.getElementById('popup_alert')){
					console.log('popup_alert', popup_alert);
					document.getElementById('popup_alert').className = 'alert';
					console.log('popup_alert', popup_alert);
				}
				if(tout != null){
					clearTimeout(tout);				
				}
			};
		}
	}

	render() {
		return <Router basename={this.baseName}>
			<Switch>
				<Route exact path='/' component={Dashboard}/>
				<Route component={Error404} />
			</Switch>
		</Router>;
	}
}