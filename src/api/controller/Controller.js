// @flow
import _ from 'lodash';
import express from 'express';
import moment from 'moment-timezone';
const mysql = require('mysql');
const config = require('./../configs.js');
const slugify = require('slugify');

export default class Controller {
	static dbConnection : mysql.createPool;
	constructor(){
		Controller.dbConnection = config.dbConnection;
	}

	isValidJson(json_string) {
		try {
			JSON.parse(json_string);
		} catch(err) {
			return false;
		}
		return true;
	}

	queryDb(query, options) {
		return new Promise((resolve, reject) => {
			if (options) {
				Controller.dbConnection.query(query, options, (err, result) => {
					if (err) reject(err);
					else resolve(result);
				});
			} else {
				Controller.dbConnection.query(query, (err, result) => {
					if (err) reject(err);
					else resolve(result);
				});
			}
		});
	}
}