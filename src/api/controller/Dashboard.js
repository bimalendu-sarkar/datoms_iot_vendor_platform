// @flow
import express from 'express';
const mysql = require('mysql');
const config = require('./../configs.js');
import _ from 'lodash';
import moment from 'moment-timezone';

export default class Dashboard {
	statusDashboard(req, res) {
		res.status(200).json({
			status: 'success',
			data: moment()
		});
	}
}