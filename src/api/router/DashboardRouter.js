//@flow

import express from 'express';
import Dashboard from './../controller/Dashboard';
const multer = require('multer');
const user_id = '1';
const authenticate = require('./../authenticate.js');
const path = require('path');

let storage = multer.diskStorage({
	destination: function (req, file, cb) {
	  cb(null, 'uploads')
	},
	filename: function (req, file, cb) {
		console.log('file name is ',file);
		let filename = file.originalname.split('.');
		req.filename = filename[0]+'-'+req.user.ui+ '-' + Date.now()+'.'+filename[1];
	  cb(null, req.filename);
	}
})

export default class DashboardRouter {
	constructor(){
		this.router = express.Router();
		this.dashboard = new Dashboard();
		this.upload = multer({storage:storage});
		this.routes();
	};

	routes() {
		/**
		 * @api {get} /status Get Status
		 * @apiVersion 0.1.0
		 * @apiName statusDashboard
		 * @apiGroup Dashboard
		 * 
		 * @apiSuccess {String} status Status of the Query
		 * @apiSuccess {String} data Response Data
		 * @apiSuccessExample 200 Response
		 * HTTP/1.1 200 OK
		 * {
		 * 	"status": "success",
		 * 	"data": ""
		 * }
		 *
		 * @apiError 400 Some Required Parameters are not provided
		 * @apiErrorExample 400 Response
		 * HTTP/1.1 400 Bad Request
		 * {
		 * 	"status": "failure",
		 * 	"message": "Please provide all required data!"
		 * }
		 *
		 * @apiError 404 API Not Available
		 * @apiErrorExample 404 Response
		 * HTTP/1.1 404 Not Found
		 * {
		 * 	"status": "failure",
		 * 	"message": "Unable to fetch data!"
		 * }
		 *
		 * @apiError 401 Unauthorized User
		 * @apiErrorExample 401 Response
		 * HTTP/1.1 401 Unauthorized
		 *
		 * @apiError 500 Error occurred in API
		 * @apiErrorExample 500 Response
		 * HTTP/1.1 500 Internal Server Error
		 * {
		 * 	"status": "failure",
		 * 	"message": "Something went wrong!",
		 * 	"error": "<Error Message>"
		 * }
		*/

		this.router.get('/status', authenticate, (req, res) => {
			this.dashboard.statusDashboard(req, res);
		});
	}
}
