const path = require('path'),
	env = require('dotenv').config({path: path.resolve(__dirname, './.env')});
process.env = ((env && env.parsed) ? env.parsed : {});

const build_mode = process.env.NODE_ENV || 'development';
const build_type = process.env.BUILD_TYPE || 'local';
const library_paths = require('./'+ build_type);

module.exports = {
	dbConnection: require(library_paths.db_connection),
	libraryACL: require(library_paths.acl_library)
}