//@flow
import express from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import DashboardRoute from './router/DashboardRouter.js';
const cors = require('cors');

const requestOptions = {
	origin: function (origin :String , callback) {
		callback(null, true)
	},
	methods: ['OPTIONS','GET','POST'],
	credentials: true
};
const version = 'v##PR_BACKEND_API_VERSION##';

export default class App {
	constructor() {
		this.express = express();
		this.dashboardRoute = new DashboardRoute();
		this.middleware();
		this.routes();
	}

	middleware() {
		this.express.use(morgan('dev'));
		this.express.use(bodyParser.json());
		// this.express.use(bodyParser.urlencoded({extended: false}));
		this.express.use(cors(requestOptions));
	}

	routes() {
		this.express.use('/-/api/demo/' + version + '/dashboard',this.dashboardRoute.router);
	}
}