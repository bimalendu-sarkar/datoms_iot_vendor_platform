const mysql = require('mysql');
const config = require('./configs.js');
const path = require('path'),
	env = require('dotenv').config({path: path.resolve(__dirname, './.env')});
process.env = ((env && env.parsed) ? env.parsed : {});
console.log('authenticate process.env', JSON.stringify(process.env));
const connection = config.dbConnection;

var test_authenticate = (req, res, done) => {
	req.user = {
		un: 'Admin',
		user_email_id: 'admin@phoenixrobotix.com',
		ui: 1,
		ut: 'admin',
		oi: 1,
		org_name: 'Phoenix Robotix',
		hp: '//datoms.phoenixrobotix.com',
		is_admin: 1
	};
	return done();
}

var authenticate = (req, res, done)=>{
	var msg = 'Unauthorized Access!';
	var session = '';
	try{
		var cookie = (req.headers && req.headers.cookie) ? req.headers.cookie.split('; ') : [];
		cookie.map((item) => {
			if (item && item.split('=')[0] == 'PHPSESSID') {
				session = item.split('=')[1];
			}
		});
	}catch(err){
		res.status(401).send(JSON.stringify({
			status: 'error',
			message: msg,
			error: err.message
		}));
		return;
	}
	
	// console.log('Session:', session);
	connection.query({
		sql: 'SELECT * FROM `phoenzbi_services`.`usr_sessions` WHERE `usrs_session_id`=? AND `usrs_logout_time`=?',
		values: [session, 0],
		timeout: 30000
	}, (con_err, con_result, con_fields) => {
		// And done with the connection.
		// connection.end();
		// Handle error after the release.
		if (con_err) {
			res.set({'error': con_err.message}).status(500).json({
				status: 'failure',
				message: 'User not logged in!',
				error: (req.user.ui == 1) ? JSON.stringify(con_err) : undefined
			});
			return;
		}
		// console.log(session, con_result);
		if (con_result && con_result.length && con_result[0] && session == con_result[0].usrs_session_id) {
			var user_data = JSON.parse(con_result[0].usrs_session_data);
			// console.log('Result Session:', user_data);
			// getData(user_data);
			req.user = user_data;
			done();
		} else {
			console.log(msg);
			res.status(401).send(JSON.stringify({
				status: 'error',
				message: msg
			}));
			return;
		}
	});
}

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
	module.exports = test_authenticate;
} else if (process.env.NODE_ENV === 'production') { 
	module.exports = authenticate;
}