const gulp = require('gulp'),
	webpack = require('webpack'),
	path = require('path'),
	fs = require('fs'),
	webpack_dev_server = require('webpack-dev-server'),
	gutil = require('gulp-util'),
	nodemon = require('nodemon'),
	gulp_jest = require('gulp-jest').default,
	watch = require('gulp-watch'),
	runSequence = require('run-sequence'),
	rimraf = require('rimraf'),
	string_replace = require('gulp-string-replace'),
	htmlmin = require('gulp-htmlmin'),
	rename = require('gulp-rename'),
	sri = require('gulp-sri'),
	hash = require('gulp-hash'),
	ifElse = require('gulp-if-else'),
	pjson = require('./package.json'),
	clean = require('gulp-clean'),
	babel = require('gulp-babel'),
	stripDebug = require('gulp-strip-debug'),
	uglify = require('gulp-uglify');

// Set variables through Node Environment variable
const build_mode = process.env.NODE_ENV || 'development';
// const build_mode = 'production';
const build_type = process.env.BUILD_TYPE || 'local';
// const build_type = 'stage';
// const build_type = 'deploy';
const backend_port = process.env.npm_package_config_port || 9091;

//cdn base
const cdn_base = (build_type === 'deploy' ? 'https://prstatic.phoenixrobotix.com/datoms/demo/' + (pjson.version ? ('v' + pjson.version + '/') : '') : 'https://dev.datoms.io/datoms/demo/cdn/');

let onBuild = (done) => {
	return (err, stats) => {
		if (err) {
			console.log('Error', err);
		} else {
			console.log(stats.toString());
		}
		if (done) {
			done();
		}
	};
};

// front end task

gulp.task('frontend', (done) => {
	let webpack_config_app = require('./webpack.config.js');
	// console.log(webpack_config_app);

	if (build_mode === 'production') {
		webpack(webpack_config_app).run(onBuild(done));
	} else if (build_mode === 'development') {
		new webpack_dev_server(webpack(webpack_config_app), {
			publicPath: webpack_config_app.output.publicPath,
			contentBase:'./build/app',
			stats: {
				colors: true
			}
		}).listen(9090,'localhost', (err) => {
			if (err) throw new gutil.PluginError('webpack-dev-server', err);
			gutil.log('[webpack-dev-server]', 'http://127.0.0.1:9090/webpack-dev-server/index.html');
		});
	}
});

//php file
gulp.task('php_file', () => {
	setTimeout(() => {
		let src = 'src/app/index.html';
		let files_sri = JSON.parse(fs.readFileSync('./build/app/cdn/sri.json', 'utf8'));
		let hashed_file_names = JSON.parse(fs.readFileSync('./build/app/cdn/hash_manifest.json', 'utf8'));
		let js_preload_link = '<link as="script" href="'+ cdn_base + hashed_file_names['bundle.js'] +'" rel="preload" />';
		let js_file_link = '<script src="'+ cdn_base + hashed_file_names['bundle.js'] +'" integrity="'+ files_sri['build/app/bundle.js'] +'" crossorigin="anonymous"></script>';
		// let js_file_link = '<script src="'+ cdn_base + hashed_file_names['bundle.js'] +'"></script>';
		let backend_content_inside_head = js_preload_link;
		let backend_content_at_end_of_body = (build_type === 'deploy') ? fs.readFileSync('./src/app/php_content.php', 'utf8') : '';
		backend_content_at_end_of_body += js_file_link;

		gulp
			.src(src)
			.pipe(
				string_replace('<!-- ##PR_REPLACE_BY_BACKEND_CONTENT_INSIDE_HEAD## -->', backend_content_inside_head)
			)
			.pipe(
				string_replace('<!-- ##PR_REPLACE_BY_BACKEND_CONTENT_BEFORE_BODY_END## -->', backend_content_at_end_of_body)
			)
			.pipe(
				htmlmin({
					removeComments: true,
					collapseWhitespace: true,
					collapseBooleanAttributes: true,
					// removeTagWhitespace: true,
					removeAttributeQuotes: true,
					removeRedundantAttributes: true,
					removeEmptyAttributes: true,
					removeScriptTypeAttributes: true,
					removeStyleLinkTypeAttributes: true/*,
					minifyJS: true,
					processScripts: ['text/javascript'],
					minifyCSS: true,*/
					// ignoreCustomFragments: true/* use this to conserve php */
				})
			)
			.pipe(
				rename('index.php')
			)
			.pipe(
				gulp.dest('./build/app')
			);
	}, 2000);
});

//static assets
//generate hash
gulp.task('generate_hashes_for_js_file', () => {
	rimraf('./build/app/cdn/', [], () => {
		gulp
			.src([
				'./build/app/bundle.js'
			])
			.pipe(
				hash({
					algorithm: 'sha512',
					hashLength: 64
				})
			)
			.pipe(
				gulp.dest('./build/app/cdn/')
			)
			.pipe(
				hash.manifest('hash_manifest.json')
			)
			.pipe(
				gulp.dest('./build/app/cdn/')
			);
	});
});
//generate sri
gulp.task('generate_sri_for_js_file', () => {
	gulp
		.src([
			'./build/app/bundle.js'
		])
		.pipe(
			sri({
				algorithms: ['sha384']
			})
		)
		.pipe(
			gulp.dest('./build/app/cdn/')
		)
});

// Change the React Router
gulp.task('react_browser_router', () => {
	gulp
		.src(['./src/app/js/App.js'])
		.pipe(string_replace('HashRouter', 'BrowserRouter'))
		.pipe(gulp.dest('./src/app/js/'))
});
gulp.task('react_hash_router', () => {
	gulp
		.src(['./src/app/js/App.js'])
		.pipe(string_replace('BrowserRouter', 'HashRouter'))
		.pipe(gulp.dest('./src/app/js/'));
	gulp.src(['./build/app/bundle.js', './build/app/cdn/hash_manifest.json', './build/app/cdn/sri.json'])
		.pipe(clean());
});

// backend tasks

gulp.task('clean-api', () => {
	gulp.src(['./build/api/**/*.js', './build/api/*.library_paths'])
		.pipe(clean());
});

gulp.task('build-api', ['clean-api'], () => {
	gulp.src(['./src/api/**/*.js', './src/api/'+ build_type +'.library_paths'])
		.pipe(babel({
			'presets': [
				'@babel/env',
				'@babel/flow'
			],
			'plugins': [
				'@babel/plugin-transform-runtime'
			]
		}))
		.on('error', (err) => {
			console.log('babel > ', err);
		})
		.pipe(
			string_replace('##PR_BACKEND_API_VERSION##', pjson.version)
		)
		.pipe(
			ifElse(build_mode === 'production', () => stripDebug())
		)
		.pipe(
			ifElse(build_mode === 'production', () => uglify())
		)
		.pipe(
			gulp.dest('build/api')
		);
});

gulp.task('build-backend', ['build-api'], () => {
	if (build_mode === 'development') {
		nodemon.restart();		
	}
});

// watch tasks
gulp.task('backend-watch', () => {
	return watch('./src/api/*/**', () => {
		gulp.run('build-backend');
	});
});

// final build
gulp.task('backend', ['build-backend'], () => {
	if (build_mode === 'development') {
		setTimeout(() => {
			nodemon({
				execMap: {
					js: 'node'
				},
				script: path.resolve(__dirname, './build/api/index.js'),
				ignore: ['*'],
				ext: 'noop'
			}).on('restart', () => {
				console.log('Backend Restarted!');
			}).on('error', (error) => {
				console.log(error);
				nodemon.restart();
			});
		}, 1500);
		runSequence('backend-watch');
	} else {
		gulp
			.src('./src/api/package.json')
			.pipe(gulp.dest('./build/api/'));
		gulp
			.src('./src/api/'+ build_type +'.env')
			.pipe(string_replace('##PORT##', backend_port))
			.pipe(rename('.env'))
			.pipe(gulp.dest('./build/api/'));
	}
});