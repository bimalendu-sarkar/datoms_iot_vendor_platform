'use strict';

var moment = require('moment-timezone');
var mysql = require('mysql');
var random = require('randomatic');

let config = {
	host: '127.0.0.1',
	user: 'root',
	password: '',
	database: 'phoenzbi_data',
	port: 3306,
	connectionLimit: 10
};

let db_pool = mysql.createPool(config);

function getRandomFloat(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return parseFloat(((Math.random() * (max - min)) + min).toFixed(2));
}

function getRandom(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return parseInt(Math.floor(Math.random() * (max - min)) + min);
}

let isValidJson = function(json_string){
	try{
		JSON.parse(json_string);
		return true;
	}catch(err){
		return false;
	}
}

let queryDb = function(query,options){
	let self = this; 
	return new Promise((resolve,reject)=>{
		if(options){
			db_pool.query(query,options,(err,result)=>{
				if(err) reject(err);
				else resolve(result);
			});
		}else{
			db_pool.query(query,(err,result)=>{
				if(err) {
					console.log(err.message);
					reject(err);
				}else {
					resolve(result);
					// Controller.dbConnection.release();					
				};
			});
		}
	});
}

let generateEvents = function(station_id, template_id, rule_id, client_id, event_time, data_set){
	queryDb('SELECT art_alert_templates FROM automated_rule_template WHERE art_id = ? ',[template_id]).then((template_json)=>{
		let template_list = ((isValidJson(template_json[0].art_alert_templates))? JSON.parse(template_json[0].art_alert_templates) : []);

	// console.log("generateEvents template_json >> ", template_json[0].art_alert_templates);
	// console.log("generateEvents isValidJson(template_json.art_alert_templates) >> ", isValidJson(template_json[0].art_alert_templates));
	// console.log("generateEvents template_list >> ", template_list);
	

		if(template_list.length){
			let message_template_id;
			template_list.map((template)=>{
				if(template.mode == 'webapp'){
					message_template_id = template.template_id;
				}
			});

	

			if(message_template_id){
				queryDb('SELECT dat_template FROM datoms_alert_templates WHERE dat_id = ?', message_template_id).then((result)=>{
					
					if(result.length){

						let message_template = result[0].dat_template;
						let message = message_template;
						Object.keys(data_set).map((key)=>{
							let variable = '@'+key+'@';
							message_template.split(variable).map(()=>{
								message = message.replace(variable,data_set[key]);
							});
						});

						queryDb('INSERT INTO events_table (`rule_id`, `serv_id`, `clep_id`, `event_generation_time`, `event_station_id`, `event_message`, `event_details`) VALUES (?, ?, ?, ?, ?, ?, ? )', [rule_id, 18, client_id, event_time, station_id, message, (JSON.stringify(data_set))]).catch((err)=>{
							console.log('<---------- Error Generated in generateEvents ----------->');
							console.log('3. Error is >', err.message);
							console.log('<------------------------ XXX -------------------------->');
						});
					}

				}).catch((err)=>{
					console.log('<---------- Error Generated in generateEvents ----------->');
					console.log('2. Error is >', err.message);
					console.log('<------------------------ XXX -------------------------->');
				});
			}
		}
	}).catch((err)=>{
		console.log('<---------- Error Generated in generateEvents ----------->');
		console.log('1. Error is >', err.message);
		console.log('<------------------------ XXX -------------------------->');
	})
}

let genrateStationStatus = function(station_id, station_name, client_id, start_time){
	
	let template_id = 5;
	let rule_id = 10;
	let current_time = parseInt(moment.tz('Asia/Kolkata').format('X'));
	start_time = parseInt(start_time);
	let event_time = start_time;

	let previous_status = 'offline';
	
	while(event_time < current_time){
		let get_intrval = getRandom(3600, 86400);
		event_time = event_time + get_intrval;

		if(previous_status == 'offline'){
			let data_set = {
				name: station_name,
				status: 'online'
			}
			previous_status = 'online';

			generateEvents(station_id, template_id, rule_id, client_id, event_time, data_set);
		}else if(previous_status == 'online'){
			let ismaintenance = (getRandom(9, 100) % 2);
			let data_set ={};
			if(ismaintenance){
				queryDb('UPDATE cold_storage_monitoring_stations SET csms_is_maintenance = 1 WHERE dstn_id = ? ', [station_id]).catch((err)=>{
					console.log('<---------- Error Generated in genrateStationStatus ----------->');
					console.log('1. Error is >', err.message);
					console.log('<------------------------ XXX -------------------------->');
				});
				data_set = {
					name: station_name,
					status: 'under maintenance'
				}
				previous_status = 'under maintenance';
			}else{
				
				data_set = {
					name: station_name,
					status: 'offline'
				}
				previous_status = 'online';
			}
			generateEvents(station_id, template_id, rule_id, client_id, event_time, data_set);
		}else if(previous_status == 'under maintenance'){
			let data_set = {
				name: station_name,
				status: 'online'
			}
			previous_status = 'online';
			queryDb('UPDATE cold_storage_monitoring_stations SET csms_is_maintenance = 0 WHERE dstn_id = ? ', [station_id]).catch((err)=>{
				console.log('<---------- Error Generated in genrateStationStatus ----------->');
				console.log('2. Error is >', err.message);
				console.log('<------------------------ XXX -------------------------->');
			});
			generateEvents(station_id, template_id, rule_id, client_id, event_time, data_set);
		}
	}
};

let checkStationViolation = function(station_id, station_name, client_id, param_details, dataset, data_time){
	
	let template_id = 4;
	let rule_id = 9;
	
	let violations_data = [];

	// @name@ have violated the @violation_var@ limit. Station temperature in @value@ while @violation_var@ limit is set to @violation_var_value@

	param_details.map((param)=>{
		let data = dataset[param.key];
		let threshold = param.threshold;
		if(parseInt(data) > parseInt(threshold.max)){
			violations_data.push({
				name: station_name,
				violation_var: 'maximum',
				violation_var_value: threshold.max,
				value: data
			});
		}else if(parseInt(data) < parseInt(threshold.min)){
			violations_data.push({
				name: station_name,
				violation_var: 'minimum',
				violation_var_value: threshold.min,
				value: data
			});
		}
	});

	if(violations_data.length){
		violations_data.map((violation_data_set)=>{
			generateEvents(station_id, template_id, rule_id, client_id, data_time, violation_data_set);
		});
	}
};

let generateStationRawData = function(station_id, station_name, client_id, start_time, param_details){
	
	let parameter_details = param_details;
	start_time = parseInt(start_time);
	let event_time = start_time;
	let current_time = parseInt(moment.tz('Asia/Kolkata').format('X'));
	
	if(parameter_details.length){

		while(event_time < current_time){

			let data = {};
			
			parameter_details.map((param)=>{
				data[param.key] = getRandom(-20, 10);
			});
			
			checkStationViolation(station_id, station_name, client_id, parameter_details, data, event_time);

			queryDb('INSERT INTO `cold_storage_monitoring_stations_raw_data` (`dstn_id`, `csmsrd_time`, `csmsrd_data`) VALUES (?,?,?)',[station_id, event_time, JSON.stringify(data)]).catch((err)=>{
				console.log('<---------- Error Generated in generateStationRawData ----------->');
				console.log('1. Error is >', err.message);
				console.log('<------------------------ XXX -------------------------->');
			});
			
			queryDb('UPDATE datoms_stations SET dstn_last_data = ? , dstn_last_data_receive_time = ? WHERE dstn_id = ?', [JSON.stringify(data), event_time, station_id]).catch((err)=>{
				console.log('<---------- Error Generated in generateStationRawData ----------->');
				console.log('2. Error is >', err.message);
				console.log('<------------------------ XXX -------------------------->');
			});

			let get_intrval = getRandom(30, 3600);
			
			event_time = event_time + get_intrval;
		}

	}

};

let generateStationAvgData = function(station_id, station_name, client_id, start_time, param_details){
	let parameter_details = param_details;
	start_time = parseInt(start_time);
	let current_time = parseInt(moment.tz('Asia/Kolkata').format('X'));
	
	if(parameter_details.length){
		
		let min_interval = start_time, hourly_interval = start_time, daily_interval = start_time;

		while(min_interval < current_time){

			let data = {};
			parameter_details.map((param)=>{
				data[param.key] ={
					avg: 0,
					min: 0,
					max: 0,
					min_at: 0,
					max_at: 0
				};

				data[param.key].max = getRandom( -10 , 10);
				data[param.key].min = getRandom( -20 , -10);
				data[param.key].avg = getRandom(data[param.key].min, data[param.key].max);
				data[param.key].min_at = getRandom(min_interval, min_interval + 900);
				data[param.key].max_at = getRandom(min_interval, min_interval + 900);
	
			});
			
			queryDb('INSERT INTO `cold_storage_monitoring_stations_15min_data`(`dstn_id`, `csms15d_time`, `csms15d_data`) VALUES (?,?,?)',[station_id, (min_interval + 900), JSON.stringify(data)]).catch((err)=>{
				console.log('<---------- Error Generated in generateStationAvgData ----------->');
				console.log('Error is >', err.message);
				console.log('<------------------------ XXX -------------------------->');
			});

			min_interval = min_interval + 900;
		}

		while(hourly_interval < current_time){

			let data = {};
			parameter_details.map((param)=>{
				data[param.key] ={
					avg: 0,
					min: 0,
					max: 0,
					min_at: 0,
					max_at: 0
				};

				data[param.key].max = getRandom( -10 , 10);
				data[param.key].min = getRandom( -20 , -10);
				data[param.key].avg = getRandom(data[param.key].min, data[param.key].max);
				data[param.key].min_at = getRandom(hourly_interval, hourly_interval + 3600);
				data[param.key].max_at = getRandom(hourly_interval, hourly_interval + 3600);
	
			});
			
			queryDb('INSERT INTO `cold_storage_monitoring_stations_hourly_data`(`dstn_id`, `csmshd_time`, `csmshd_data`) VALUES (?,?,?)',[station_id, (hourly_interval + 3600), JSON.stringify(data)]).catch((err)=>{
				console.log('<---------- Error Generated in generateStationAvgData ----------->');
				console.log('Error is >', err.message);
				console.log('<------------------------ XXX -------------------------->');
			});
			
			hourly_interval = hourly_interval + 3600;

		}

		while(daily_interval < current_time){
			
			let data = {};
			parameter_details.map((param)=>{
				data[param.key] ={
					avg: 0,
					min: 0,
					max: 0,
					min_at: 0,
					max_at: 0
				};

				data[param.key].max = getRandom( -10 , 10);
				data[param.key].min = getRandom( -20 , -10);
				data[param.key].avg = getRandom(data[param.key].min, data[param.key].max);
				data[param.key].min_at = getRandom(daily_interval, daily_interval + 86400);
				data[param.key].max_at = getRandom(daily_interval, daily_interval + 86400);
	
			});
			
			queryDb('INSERT INTO `cold_storage_monitoring_stations_daily_data`(`dstn_id`, `csmsdd_time`, `csmsdd_data`) VALUES (?,?,?)',[station_id, (daily_interval + 86400), JSON.stringify(data)]).catch((err)=>{
				console.log('<---------- Error Generated in generateStationAvgData ----------->');
				console.log('Error is >', err.message);
				console.log('<------------------------ XXX -------------------------->');
			});
			
			daily_interval = daily_interval + 86400;

		}

	}
		
};

let pushDummyData = function(){
	queryDb('SELECT dstn_id, dstn_name, dstn_param_details, clep_id, dstn_added_at FROM datoms_stations WHERE serv_id = 18').then((results)=>{
		results.map((result)=>{
			let param_details = ((isValidJson(result.dstn_param_details))? JSON.parse(result.dstn_param_details) : {});
			genrateStationStatus(result.dstn_id, result.dstn_name, result.clep_id, result.dstn_added_at);
			generateStationRawData(result.dstn_id, result.dstn_name, result.clep_id, result.dstn_added_at, param_details);
			generateStationAvgData(result.dstn_id, result.dstn_name, result.clep_id, result.dstn_added_at, param_details);
		});
	}).catch((err)=>{
		console.log('<---------- Error Generated in pushDummyData ----------->');
		console.log('Error is >', err.message);
		console.log('<------------------------ XXX -------------------------->');
	});
}

pushDummyData();