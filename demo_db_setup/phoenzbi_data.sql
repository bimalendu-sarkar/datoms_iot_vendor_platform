SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;

CREATE TABLE `alert_rules` (
  `rule_id` bigint(20) UNSIGNED NOT NULL,
  `art_id` bigint(20) UNSIGNED NOT NULL,
  `ur_id` bigint(20) UNSIGNED NOT NULL,
  `clep_id` bigint(20) UNSIGNED NOT NULL,
  `serv_id` bigint(20) UNSIGNED NOT NULL,
  `rule_station_id` bigint(20) UNSIGNED NOT NULL,
  `idev_id` bigint(20) UNSIGNED NOT NULL,
  `stgr_id` bigint(20) UNSIGNED NOT NULL,
  `rule_actions` text NOT NULL,
  `rule_is_active` tinyint(1) UNSIGNED NOT NULL COMMENT '1 -> active, 0 -> inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `alert_rules` VALUES(1, 1, 2, 365, 19, 1, 0, 0, '[   {     \"type\": \"alert\",     \"modes\": [       {         \"mode\": \"email\"       }     ]   } ]', 1);
INSERT INTO `alert_rules` VALUES(2, 2, 2, 365, 19, 0, 0, 1, '[   {     \"type\": \"alert\",     \"modes\": [       {         \"mode\": \"email\"       }     ]   } ]', 1);
INSERT INTO `alert_rules` VALUES(3, 2, 2, 365, 19, 5, 0, 0, '[   {     \"type\": \"alert\",     \"modes\": [       {         \"mode\": \"email\"       }     ]   } ]', 1);
INSERT INTO `alert_rules` VALUES(4, 1, 2, 365, 19, 0, 0, 1, '', 1);
INSERT INTO `alert_rules` VALUES(5, 3, 2, 365, 19, 1, 0, 0, '', 1);
INSERT INTO `alert_rules` VALUES(6, 3, 2, 365, 19, 1, 0, 0, '', 0);
INSERT INTO `alert_rules` VALUES(7, 3, 2, 365, 19, 0, 0, 1, '', 1);
INSERT INTO `alert_rules` VALUES(8, 1, 2, 365, 19, 4, 0, 0, '', 1);
INSERT INTO `alert_rules` VALUES(9, 4, 0, 2, 18, 0, 0, 0, '', 1);
INSERT INTO `alert_rules` VALUES(10, 5, 0, 2, 18, 0, 0, 0, '', 1);

CREATE TABLE `automated_rule_template` (
  `art_id` bigint(20) UNSIGNED NOT NULL,
  `clep_id` bigint(20) UNSIGNED NOT NULL,
  `serv_id` bigint(20) UNSIGNED NOT NULL,
  `art_name` varchar(255) NOT NULL,
  `art_defination` text NOT NULL,
  `art_description` varchar(255) NOT NULL,
  `art_alert_templates` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `automated_rule_template` VALUES(1, 365, 19, 'Template 1', '', 'This is the first template', '[\r\n  {\r\n    \"mode\": \"email\",\r\n    \"template_id\": 1\r\n  }\r\n]');
INSERT INTO `automated_rule_template` VALUES(2, 365, 19, 'Template 2', '[\r\n  {\r\n    \"type\": \"penstock_level\",\r\n    \"expression\": \" @value@ > @max@ \"\r\n  },\r\n  {\r\n    \"type\": \"sump_level\",\r\n    \"expression\": \"@value@ > @max@\"\r\n  }\r\n]', 'This is the second template', '[\r\n  {\r\n    \"mode\": \"email\",\r\n    \"template_id\": 1\r\n  }\r\n]');
INSERT INTO `automated_rule_template` VALUES(3, 365, 19, 'Template 3', '', 'This is the third template', '[\r\n  {\r\n    \"mode\": \"email\",\r\n    \"template_id\": 1\r\n  }\r\n]');
INSERT INTO `automated_rule_template` VALUES(4, 2, 18, 'Violation Template', '[\r\n  {\r\n    \"type\": \"storage_temperature\",\r\n    \"expression\": \" @value@ > @max@ \"\r\n  },\r\n  {\r\n    \"type\": \"storage_temperature\",\r\n    \"expression\": \"@value@ < @min@\"\r\n  }\r\n]', 'This is the template for violations', '[\r\n  {\r\n    \"mode\": \"webapp\",\r\n    \"template_id\": 2\r\n  }\r\n]');
INSERT INTO `automated_rule_template` VALUES(5, 2, 18, 'Status Template', '', 'This is the template is to check for table status', '[\r\n  {\r\n    \"mode\": \"webapp\",\r\n    \"template_id\": 3\r\n  }\r\n]');

CREATE TABLE `client_end_point_tbl` (
  `clep_id` bigint(20) UNSIGNED NOT NULL,
  `clep_name` varchar(100) NOT NULL,
  `clep_slug` text NOT NULL,
  `clsbcat_id` bigint(20) UNSIGNED NOT NULL,
  `clep_type` tinyint(1) UNSIGNED NOT NULL,
  `clep_tin` text NOT NULL,
  `clep_status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `clep_status_chng_at` bigint(10) UNSIGNED NOT NULL,
  `clep_scale` tinyint(1) UNSIGNED DEFAULT NULL,
  `clgrp_id` text NOT NULL,
  `clep_org_purpose` text NOT NULL,
  `clep_lat` double NOT NULL,
  `clep_long` double NOT NULL,
  `area_id` bigint(20) DEFAULT NULL,
  `clep_ho_addr_plotno` varchar(50) NOT NULL,
  `clep_ho_addr_street` varchar(100) NOT NULL,
  `clep_ho_addr_area` varchar(100) NOT NULL,
  `clep_ho_addr_city` varchar(50) NOT NULL,
  `clep_ho_addr_state` varchar(20) NOT NULL,
  `clep_ho_addr_pin` mediumint(6) DEFAULT NULL,
  `clep_ho_addr_country` varchar(20) NOT NULL,
  `clep_plant_addr_plotno` varchar(256) NOT NULL,
  `clep_plant_addr_street` varchar(100) NOT NULL,
  `clep_plant_addr_area` varchar(100) NOT NULL,
  `clep_plant_addr_city` varchar(50) NOT NULL,
  `clep_plant_addr_state` varchar(20) NOT NULL,
  `clep_plant_addr_pin` mediumint(6) UNSIGNED DEFAULT NULL,
  `clep_plant_addr_country` varchar(20) NOT NULL,
  `clep_phones` text NOT NULL,
  `clep_emails` text NOT NULL,
  `clep_website` varchar(254) NOT NULL,
  `clep_comments` text NOT NULL,
  `clep_attachments` text NOT NULL,
  `clep_added_by` bigint(20) UNSIGNED NOT NULL,
  `clep_added_at` bigint(10) UNSIGNED NOT NULL,
  `clep_added_from_ua` text NOT NULL,
  `clep_removed_by` bigint(20) UNSIGNED NOT NULL,
  `clep_removed_at` bigint(10) UNSIGNED NOT NULL,
  `clep_removed_from_ua` text NOT NULL,
  `cleppltnzn_id` bigint(20) UNSIGNED NOT NULL,
  `clep_is_vendor` tinyint(3) UNSIGNED NOT NULL COMMENT '0 -> No, 1 -> Yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `client_end_point_tbl` VALUES(2, 'Demo', 'Demo-demo', 4, 1, '', 1, 1580588477, 2, '[]', '', 22.25013, 84.89985, 2, '', '', '', '', '', NULL, '', '', 'Sector 1', 'Sector 1', 'Rourkela', 'Odisha', 769008, 'India', '', '', 'http://phoenixrobotix.com', '', '', 0, 0, '', 0, 0, '', 0, 0);
INSERT INTO `client_end_point_tbl` VALUES(365, 'Phoenix Robotix', 'phoenix-robotix', 4, 1, '', 1, 1480588477, 2, '[]', '', 22.25013, 84.89985, 1, '', '', 'Sector 1', 'Rourkela', 'Odisha', 769008, 'India', '', '', 'Sector 1', 'Rourkela', 'Odisha', 769008, 'India', '[\"\"]', '[\"\"]', 'http://phoenixrobotix.com', '', '', 0, 0, '', 0, 0, '', 11, 0);

CREATE TABLE `cold_storage_monitoring_stations` (
  `dstn_id` bigint(20) UNSIGNED NOT NULL,
  `csms_is_maintenance` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `cold_storage_monitoring_stations` VALUES(1, 1);
INSERT INTO `cold_storage_monitoring_stations` VALUES(2, 1);
INSERT INTO `cold_storage_monitoring_stations` VALUES(3, 0);
INSERT INTO `cold_storage_monitoring_stations` VALUES(4, 0);
INSERT INTO `cold_storage_monitoring_stations` VALUES(5, 0);
INSERT INTO `cold_storage_monitoring_stations` VALUES(6, 0);

CREATE TABLE `cold_storage_monitoring_stations_15min_data` (
  `dstn_id` bigint(20) UNSIGNED NOT NULL,
  `csms15d_time` bigint(10) UNSIGNED NOT NULL,
  `csms15d_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `cold_storage_monitoring_stations_daily_data` (
  `dstn_id` bigint(20) UNSIGNED NOT NULL,
  `csmsdd_time` bigint(10) UNSIGNED NOT NULL,
  `csmsdd_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `cold_storage_monitoring_stations_hourly_data` (
  `dstn_id` bigint(20) NOT NULL,
  `csmshd_time` bigint(10) NOT NULL,
  `csmshd_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `cold_storage_monitoring_stations_raw_data` (
  `dstn_id` bigint(20) UNSIGNED NOT NULL,
  `csmsrd_time` bigint(10) UNSIGNED NOT NULL,
  `csmsrd_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `datoms_alert_templates` (
  `dat_id` bigint(20) UNSIGNED NOT NULL,
  `dat_template` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `datoms_alert_templates` VALUES(1, '\"The problem is that the value of @param@ is more than @max@ which is @value@  \"');
INSERT INTO `datoms_alert_templates` VALUES(2, '@name@ have violated the @violation_var@ limit. Station temperature in @value@ while @violation_var@ limit is set to @violation_var_value@');
INSERT INTO `datoms_alert_templates` VALUES(3, '@name@ have gone @status@');

CREATE TABLE `datoms_stations` (
  `dstn_id` bigint(20) UNSIGNED NOT NULL,
  `dstn_name` varchar(255) NOT NULL,
  `dstn_param_details` text NOT NULL,
  `serv_id` bigint(20) UNSIGNED NOT NULL,
  `clep_id` bigint(20) UNSIGNED NOT NULL,
  `dstn_latitude` double UNSIGNED NOT NULL,
  `dstn_longitude` double UNSIGNED NOT NULL,
  `dstn_last_data` text NOT NULL,
  `dstn_last_data_receive_time` bigint(10) UNSIGNED NOT NULL,
  `dstn_slug` varchar(255) NOT NULL,
  `dstn_description` text NOT NULL,
  `dstn_isactive` tinyint(1) UNSIGNED NOT NULL,
  `dstn_offline_timeout` bigint(5) UNSIGNED NOT NULL,
  `dstn_added_by` bigint(20) UNSIGNED NOT NULL,
  `dstn_added_at` bigint(10) UNSIGNED NOT NULL,
  `dstn_added_from` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `datoms_stations` VALUES(1, 'Cold Storage 1', '[\r\n  {\r\n    \"name\": \"Temperature\",\r\n    \"key\": \"temp\",\r\n    \"unit\": \"°C\",\r\n    \"device_id\": \"2\",\r\n    \"type\": \"storage_temperature\",\r\n    \"threshold\": {\r\n      \"max\": 2,\r\n      \"min\": -10\r\n    }\r\n  }\r\n]', 18, 2, 20.282198, 85.848597, '{\"temp\":-6}', 1540194122, 'cold-storage-1', '', 1, 300, 1, 1536064089, '');
INSERT INTO `datoms_stations` VALUES(2, 'Cold Storage 2', '[\r\n  {\r\n    \"name\": \"Temperature\",\r\n    \"key\": \"temp\",\r\n    \"unit\": \"°C\",\r\n    \"device_id\": \"3\",\r\n    \"type\": \"storage_temperature\",\r\n    \"threshold\": {\r\n      \"max\": 4,\r\n      \"min\": -5\r\n    }\r\n  }\r\n]', 18, 2, 20.282198, 85.848597, '{\"temp\":-6}', 1540196153, 'cold-storage-2', '', 1, 300, 1, 1536064089, '');
INSERT INTO `datoms_stations` VALUES(3, 'Cold Storage 3', '[\r\n  {\r\n    \"name\": \"Temperature\",\r\n    \"key\": \"temp\",\r\n    \"unit\": \"°C\",\r\n    \"device_id\": \"4\",\r\n    \"type\": \"storage_temperature\",\r\n    \"threshold\": {\r\n      \"max\": 4,\r\n      \"min\": 0\r\n    }\r\n  }\r\n]', 18, 2, 20.282198, 85.848597, '{\"temp\":-4}', 1540196495, 'cold-storage-3', '', 1, 300, 1, 1536064089, '');
INSERT INTO `datoms_stations` VALUES(4, 'Cold Storage 4', '[\r\n  {\r\n    \"name\": \"Temperature\",\r\n    \"key\": \"temp\",\r\n    \"unit\": \"°C\",\r\n    \"device_id\": \"4\",\r\n    \"type\": \"storage_temperature\",\r\n    \"threshold\": {\r\n      \"max\": 3,\r\n      \"min\": -17\r\n    }\r\n  }\r\n]', 18, 2, 20.282198, 85.848597, '{\"temp\":-5}', 1540195303, 'cold-storage-4', '', 1, 300, 1, 1536064089, '');
INSERT INTO `datoms_stations` VALUES(5, 'Cold Storage 5', '[\r\n  {\r\n    \"name\": \"Temperature\",\r\n    \"key\": \"temp\",\r\n    \"unit\": \"°C\",\r\n    \"device_id\": \"5\",\r\n    \"type\": \"storage_temperature\",\r\n    \"threshold\": {\r\n      \"max\": 3,\r\n      \"min\": -17\r\n    }\r\n  }\r\n]', 18, 2, 20.282198, 85.848597, '{\"temp\":-15}', 1540196203, 'cold-storage-5', '', 1, 300, 1, 1536064089, '');
INSERT INTO `datoms_stations` VALUES(6, 'Cold Storage 6', '[\r\n  {\r\n    \"name\": \"Temperature\",\r\n    \"key\": \"temp\",\r\n    \"unit\": \"°C\",\r\n    \"device_id\": \"6\",\r\n    \"type\": \"storage_temperature\",\r\n    \"threshold\": {\r\n      \"max\": 0,\r\n      \"min\": -17\r\n    }\r\n  }\r\n]', 18, 2, 20.282198, 85.848597, '{\"temp\":0}', 1539696907, 'cold-storage-6', '', 1, 300, 1, 1536064089, '');

CREATE TABLE `events_table` (
  `event_id` bigint(20) UNSIGNED NOT NULL,
  `rule_id` bigint(20) UNSIGNED NOT NULL,
  `serv_id` bigint(20) UNSIGNED NOT NULL,
  `clep_id` bigint(20) UNSIGNED NOT NULL,
  `event_generation_time` bigint(10) UNSIGNED NOT NULL,
  `event_station_id` bigint(20) UNSIGNED NOT NULL,
  `event_message` text NOT NULL,
  `event_details` text NOT NULL,
  `event_generated_by` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `events_table` VALUES(1, 1, 19, 365, 1537631213, 0, 'Parameter is more than a value', '', 2);
INSERT INTO `events_table` VALUES(2, 2, 19, 365, 1537631213, 0, 'Parameter is less than a value', '', 3);

CREATE TABLE `generated_alerts` (
  `event_id` bigint(20) UNSIGNED NOT NULL,
  `usr_id` bigint(20) UNSIGNED NOT NULL,
  `alert_media` varchar(255) NOT NULL,
  `alert_status` tinyint(2) UNSIGNED NOT NULL COMMENT '0-> failure, 1 -> pending, 2 -> success ',
  `alert_sent_at` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `generated_alerts` VALUES(1, 1, '[\'sms\']', 1, 1537681565);
INSERT INTO `generated_alerts` VALUES(1, 2, '[\"sms\",\"email\"]', 1, 1537681565);
INSERT INTO `generated_alerts` VALUES(1, 3, '[\'sms\']', 1, 1537681565);
INSERT INTO `generated_alerts` VALUES(1, 4, '[\"sms\",\"email\"]', 1, 1537681565);
INSERT INTO `generated_alerts` VALUES(2, 3, '[\'sms\']', 1, 1537681565);
INSERT INTO `generated_alerts` VALUES(2, 4, '[\"sms\",\"email\"]', 1, 1537681565);

CREATE TABLE `iot_devices` (
  `idev_id` bigint(20) NOT NULL,
  `idev_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `idev_qr_code` varchar(256) NOT NULL,
  `idev_auth_token` varchar(256) NOT NULL,
  `idev_is_active` tinyint(1) UNSIGNED NOT NULL COMMENT '0 -> No, 1 -> Yes',
  `idev_created_at` bigint(10) UNSIGNED NOT NULL,
  `idev_added_to_vendor_by` bigint(20) UNSIGNED NOT NULL,
  `idev_added_to_vendor_at` bigint(10) UNSIGNED NOT NULL,
  `idev_added_to_vendor_from_ua` text NOT NULL,
  `idev_added_to_customer_by` bigint(20) UNSIGNED NOT NULL,
  `idev_added_to_customer_at` bigint(10) UNSIGNED NOT NULL,
  `idev_added_to_customer_from_ua` text NOT NULL,
  `idct_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL,
  `industry_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `idev_imei_no` varchar(50) NOT NULL,
  `idev_firmware_version` varchar(11) NOT NULL,
  `idev_circuit_version` varchar(11) NOT NULL,
  `idt_id` tinyint(3) UNSIGNED NOT NULL,
  `last_online_time` bigint(10) UNSIGNED NOT NULL,
  `last_data_receive_time` bigint(10) UNSIGNED NOT NULL,
  `idev_data_status` tinyint(1) NOT NULL,
  `last_raw_data` text NOT NULL,
  `serv_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT '0 -> Unassigned, 1 -> Pollution Monitoring, 2 -> Energy Monitoring',
  `idev_data_sending_configurations` text NOT NULL,
  `idev_configurations` text NOT NULL,
  `idev_config_sync_msgs` text NOT NULL,
  `idev_station_list` text NOT NULL,
  `idev_latitude` float NOT NULL,
  `idev_longitude` float NOT NULL,
  `idev_configuration_changed_by` varchar(200) NOT NULL,
  `idev_configuration_changed_at` bigint(10) NOT NULL,
  `idev_error_detected_type` int(9) NOT NULL,
  `idev_error_detected_at` int(11) NOT NULL,
  `idev_status_change_time` bigint(10) DEFAULT NULL,
  `idev_deployed_at` bigint(10) DEFAULT NULL,
  `idev_manufactured_at` bigint(10) DEFAULT NULL,
  `idev_assembled_at` bigint(10) DEFAULT NULL,
  `idev_is_shutdown` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `iot_devices` VALUES(1, '', 'TR00000123456788', 'a1', 1, 0, 0, 0, '', 1, 1521200021, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', 0, 1, 365, '', '', '', 2, 1513328974, 1532529000, 1, '{\"pm25\":148,\"pm10\":184,\"temp\":8,\"humidity\":24}', 19, '', '{\"transmission_interval\":60,\"sampling_interval\":60}', '', '[16]', 21.4704, 83.9379, 'suman', 1511328974, 1, 1511327974, 1511329074, 1510029074, 1510009074, 1510019074, 1);
INSERT INTO `iot_devices` VALUES(2, '', 'TR00000123456789', 'Vu6vW0sl6l4Op9wWlPyLVXWN7SIo2DZjKxq7xVUPND60oA0bPtYGw9SuNMSyezBa', 1, 0, 0, 0, '', 1, 1521200115, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', 0, 1, 365, '', '', '', 3, 1514024627, 1532529000, 0, '{\"pm25\":126,\"pm10\":102,\"temp\":14,\"humidity\":33}', 19, '', '', '', '[0]', 22.2132, 84.7541, 'binay', 1514014627, 21, 1511318974, 1514015627, 1512015627, 1512013627, 1512014627, 0);
INSERT INTO `iot_devices` VALUES(3, '', 'TR00000123456780', 'Blnl3klmKl2aB4RK5iViaMHfnpgMEMijvrFelHDGmcnAo9qiuUbaJgexCKbLAcyi', 1, 0, 0, 0, '', 1, 1521201342, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', 0, 1, 365, '', '', '', 4, 1514024611, 1532529000, 1, '{\"pm25\":120,\"pm10\":19,\"temp\":14,\"humidity\":50}', 19, '', '', '', '[0]', 23.5318, 87.2277, 'mubaraque', 1512024611, 3, 1512022611, 1512026611, 1510026611, 1510006611, 1510016611, 1);
INSERT INTO `iot_devices` VALUES(4, '', 'TR00000123456781', 'wJKAIOJySqHJWRKLFzxx5vzJsNLyOm34xU0u12B1H60MSBf58jS4FqCC76mPttM8', 1, 0, 0, 0, '', 1, 1521211456, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', 0, 1, 365, '', '', '', 5, 1499756072, 1532529000, 0, '{\"pm25\":22,\"pm10\":273,\"temp\":8,\"humidity\":90}', 19, '', '', '', '[0]', 0, 0, 'biswa', 1499746072, 4, 1499744072, 1499746172, 1498746172, 1498726172, 1498736172, 0);
INSERT INTO `iot_devices` VALUES(5, '', 'TR00000123456782', 'AoymPFksajFhl5mddKpN9TLKHgTCZCSDZjLFEHwl2nO0A9NLh1OBpFWHdSL1nSXQ', 1, 0, 0, 0, '', 3, 1485323634, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 0, 1, 0, '', '', '', 6, 1511145016, 1532529000, 0, '{\"rain\":{\"min\":20,\"max\":52,\"avg\":42,\"min_at\":1532525910,\"max_at\":1532525422},\"pstock\":91,\"sump\":263,\"p1\":176,\"p2\":135,\"p3\":85}', NULL, '', '', '', '[0]', 23.6906, 86.0774, 'suman', 1511045016, 8, 1511044016, 1511045216, 1510045216, 1510025216, 1510035216, 1);
INSERT INTO `iot_devices` VALUES(6, '', 'TR00000123456782', 'wiZ1Rd1Sm2CSjeaYhO8W9DSXZ39cWlI0UMdYafavtFBed0DOhKs2KNoyhf8UGLLM', 1, 0, 0, 0, '', 1, 1530365476, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36', 0, 1, 0, '', '', '', 7, 1496645910, 1532529000, 0, '{\"rain\":{\"min\":42,\"max\":96,\"avg\":86,\"min_at\":1532525955,\"max_at\":1532527561},\"pstock\":241,\"sump\":281,\"p1\":206,\"p2\":37,\"p3\":259}', 0, '', '', '', '[0]', 23.6931, 86.0835, 'nago', 1496345910, 7, 1496344910, 1496346010, 1496246010, 1496226010, 1496236010, 0);
INSERT INTO `iot_devices` VALUES(7, '', 'TR00000123456782', 'PhbBMbbz1jjI4nZjrDi0T4UrDsfJKtseAr4Xmx8Dj7oXSuKb7JQf05MEIjTcl7xp', 1, 0, 0, 0, '', 3, 1485323634, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 0, 1, 365, 'ABCD1234', '4.0.0', '', 7, 1514016592, 1532529000, 0, '{\"rain\":{\"min\":33,\"max\":97,\"avg\":38,\"min_at\":1532526106,\"max_at\":1532528172},\"pstock\":111,\"sump\":85,\"p1\":48,\"p2\":115,\"p3\":160}', 19, '', '', '', '[11]', 21.1639, 77.0689, 'nago', 1514016592, 9, 1514014592, 1514016892, 1513016892, 1513000892, 1513006892, 1);
INSERT INTO `iot_devices` VALUES(8, '', 'TR00000123456782', 'JdvzPLGpIprfnv6yl51HgCGYZQYbsrF53DPzvplJtQKzbNclQl7AeZwnGvOW48DX', 1, 0, 0, 0, '', 3, 1485323634, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', 0, 1, 365, '', '1.0.0', '', 7, 1514024629, 1532529000, 1, '{\"rain\":{\"min\":6,\"max\":77,\"avg\":64,\"min_at\":1532528446,\"max_at\":1532527989},\"pstock\":57,\"sump\":50,\"p1\":20,\"p2\":158,\"p3\":105}', 19, '', '{\"communication_type\":\"1\",\"data_transmission_interval\":\"60\",\"data_sampling_interval\":\"50\",\"wifi_ssid\":\"\",\"wifi_pass\":\"\",\"ss_ip\":\"\",\"ss_port\":\"\",\"device_ip\":\"\",\"subnet_mask\":\"\",\"default_gateway\":\"\",\"dns_server\":\"\"}', '', '[15,17]', 28.5272, 77.0689, 'mubaraque', 1514014629, 5, 1514012629, 1514014929, 1513014929, 1513004929, 1513009929, 0);
INSERT INTO `iot_devices` VALUES(9, 'Demo Device 1', 'PRTD001', 'rm4cuun3D0sjwVn98PA4qhTHtrxGqE5fNB9u3uhL3oQh305zEYb1ZOPGh11QWPFP', 0, 0, 41, 1498141771, '', 41, 1498141771, '', 4, 1, 365, '', '1.0.0', '', 7, 1514024585, 1532529000, 0, '{\"w_level\":141}', 19, '', '{   \"p_status\":{     \"name\": \"Penstock Status\",     \"upper_limit\": \"120\",     \"lower_limit\": \"20\"   },   \"s_status\":{     \"name\": \"Stump Status\",     \"upper_limit\": \"80\",     \"lower_limit\": \"0\"   } }', '', '[4]', 22.2132, 83.9379, 'binay', 1513024585, 6, 1513024185, 1513024985, 1512024985, 1512014985, 1512017985, 1);
INSERT INTO `iot_devices` VALUES(10, 'Demo Device 2', 'PRTD002', 'OXu6wBEmYCspPZ6VKYKYnld6X1V2FWtUnYBbgwtvytKhMBi9vKuArvH1SQOiQowd', 1, 0, 41, 1498141771, '', 41, 1498141771, '', 7, 1, 365, '', '1.0.0', '', 4, 0, 1532529000, 1, '{\"w_level\":185}', 19, '', '{ 									\"PM2.5\":{ 										multipler:1, 										offset:0 									}, 									\"PM10\":{ 										multipler:1, 										offset:0 									}, 									\"temperature\":{ 										offset:0, 										multipler:1 									}, 									\"street_flood\":{ 										base_lable:10 									} 								}', '', '[5]', 23.6931, 77.0689, 'biswa', 0, 11, 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `iot_devices` VALUES(11, '', 'TR00000123456788', 'a1', 1, 0, 0, 0, '', 1, 1521200021, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', 0, 1, 365, '', '', '', 4, 1513328974, 1532529000, 1, '{\"inlet\":30,\"outlet\":211}', 19, '', '{\"transmission_interval\":60,\"sampling_interval\":60}', '', '[16]', 21.4704, 83.9379, 'suman', 1511328974, 1, 1511327974, 1511329074, 1510029074, 1510009074, 1510019074, 1);
INSERT INTO `iot_devices` VALUES(12, '', 'TR00000123456789', 'Vu6vW0sl6l4Op9wWlPyLVXWN7SIo2DZjKxq7xVUPND60oA0bPtYGw9SuNMSyezBa', 1, 0, 0, 0, '', 1, 1521200115, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', 0, 1, 365, '', '', '', 3, 1514024627, 1532529000, 0, '{\"inlet\":8,\"outlet\":30}', 19, '', '', '', '[0]', 22.2132, 84.7541, 'binay', 1514014627, 21, 1511318974, 1514015627, 1512015627, 1512013627, 1512014627, 0);
INSERT INTO `iot_devices` VALUES(13, '', 'TR00000123456780', 'Blnl3klmKl2aB4RK5iViaMHfnpgMEMijvrFelHDGmcnAo9qiuUbaJgexCKbLAcyi', 1, 0, 0, 0, '', 1, 1521201342, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', 0, 1, 365, '', '', '', 6, 1514024611, 1532529000, 1, '{\"w_level\":100}', 19, '', '', '', '[0]', 23.5318, 87.2277, 'mubaraque', 1512024611, 3, 1512022611, 1512026611, 1510026611, 1510006611, 1510016611, 1);
INSERT INTO `iot_devices` VALUES(14, '', 'TR00000123456781', 'wJKAIOJySqHJWRKLFzxx5vzJsNLyOm34xU0u12B1H60MSBf58jS4FqCC76mPttM8', 1, 0, 0, 0, '', 1, 1521211456, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36', 0, 1, 365, '', '', '', 8, 1499756072, 1532529000, 0, '{\"w_level\":240}', 19, '', '', '', '[0]', 0, 0, 'biswa', 1499746072, 4, 1499744072, 1499746172, 1498746172, 1498726172, 1498736172, 0);

CREATE TABLE `iot_device_types` (
  `idt_id` tinyint(3) UNSIGNED NOT NULL,
  `idt_name` varchar(50) NOT NULL,
  `idprtcl_id` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `iot_device_types` VALUES(1, 'TraDe GPRS', 1);
INSERT INTO `iot_device_types` VALUES(2, 'A', 0);
INSERT INTO `iot_device_types` VALUES(3, 'A1', 0);
INSERT INTO `iot_device_types` VALUES(4, 'B', 0);
INSERT INTO `iot_device_types` VALUES(5, 'C', 0);
INSERT INTO `iot_device_types` VALUES(6, 'D', 0);
INSERT INTO `iot_device_types` VALUES(7, 'E', 0);
INSERT INTO `iot_device_types` VALUES(8, 'F', 0);

CREATE TABLE `manual_rule_template` (
  `mat_id` bigint(20) UNSIGNED NOT NULL,
  `clep_id` bigint(20) UNSIGNED NOT NULL,
  `serv_id` bigint(20) UNSIGNED NOT NULL,
  `mat_name` varchar(255) NOT NULL,
  `mat_description` varchar(255) NOT NULL,
  `mat_template` text NOT NULL,
  `mat_created_by` bigint(20) UNSIGNED NOT NULL,
  `mat_created_at` bigint(10) UNSIGNED NOT NULL,
  `mat_created_from` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `manual_rule_template` VALUES(1, 365, 19, 'Manual Template 1', '', '{\r\n  \"template\": \" @__@ is more than @__@\",\r\n  \"var_restrictions\": [\r\n    {\r\n      \"type\":\"string\",\r\n      \"length\": 20\r\n    },\r\n    {}\r\n  ]\r\n}', 2, 1537421966, '');
INSERT INTO `manual_rule_template` VALUES(2, 365, 19, 'Manual Template 2', '', '{\r\n  \"template\": \" @__@ is less than @__@\",\r\n  \"var_restrictions\": [\r\n    {\r\n      \"type\":\"string\",\r\n      \"length\": 20\r\n    },\r\n{}\r\n    \r\n  ]\r\n}', 2, 1537421966, '');

CREATE TABLE `services_tbl` (
  `serv_id` bigint(20) UNSIGNED NOT NULL,
  `serv_name` varchar(50) NOT NULL,
  `serv_slug` varchar(50) NOT NULL,
  `latest_firmware` varchar(10) NOT NULL DEFAULT '0.0.0',
  `serv_access_list` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `services_tbl` VALUES(1, 'Pollution Monitoring', 'pollution-monitoring', '', '');
INSERT INTO `services_tbl` VALUES(2, 'Tracke', 'tracke', '', '');
INSERT INTO `services_tbl` VALUES(3, 'Weather Monitoring', 'weather-monitoring', '', '');
INSERT INTO `services_tbl` VALUES(4, 'Energy Monitoring', 'energy-monitoring', '', '');
INSERT INTO `services_tbl` VALUES(5, 'Sales', 'sales', '', '');
INSERT INTO `services_tbl` VALUES(6, 'Aurassure', 'aurassure', '', '');
INSERT INTO `services_tbl` VALUES(7, 'QC', 'qc', '', '');
INSERT INTO `services_tbl` VALUES(8, 'Purchase', 'purchase', '', '');
INSERT INTO `services_tbl` VALUES(9, 'Operations', 'operations', '', '');
INSERT INTO `services_tbl` VALUES(10, 'Sales Development', 'sales_dev', '', '');
INSERT INTO `services_tbl` VALUES(11, 'Device Management', 'device-management', '', '');
INSERT INTO `services_tbl` VALUES(12, 'IoT', 'iot', '', '');
INSERT INTO `services_tbl` VALUES(13, 'Dashboard', 'dashboard', '', '');
INSERT INTO `services_tbl` VALUES(14, 'TraDe Testing', 'trade-testing', '', '');
INSERT INTO `services_tbl` VALUES(15, 'TLP Monitoring', 'tlp-monitoring', '', '');
INSERT INTO `services_tbl` VALUES(16, 'DG Monitoring', 'dg-monitoring', '', '');
INSERT INTO `services_tbl` VALUES(17, 'IoT Platform', 'iot-platform', '', '');
INSERT INTO `services_tbl` VALUES(18, 'Cold Storage Monitoring', 'cold-storage-monitoring', '', '{   \"version\": \"1\",   \"access_groups\": [     {       \"group_name\": \"Access Data\",       \"group_description\": \"Access Data of various stations\",       \"access_list\": [         {           \"name\": \"Real-time Data\",           \"description\": \"Access Real-time Data of various stations.\",           \"key\": \"AccessData:RealTime\"         },         {           \"name\": \"Historical Data\",           \"description\": \"\",           \"key\": \"AccessData:HistoricalData\"         }       ]     },     {       \"group_name\": \"Advanced Analytics\",       \"group_description\": \"Access Advanced Analytics of various stations.\",       \"access_list\": [         {           \"name\": \"Forecasting\",           \"description\": \"\",           \"key\": \"AdvancedAnalytics:Forecasting\"         },         {           \"name\": \"Scenarios\",           \"description\": \"\",           \"key\": \"AdvancedAnalytics:Scenarios\"         }       ]     },     {       \"group_name\": \"Receive Alerts\",       \"group_description\": \"Receive Alerts for assigned events.\",       \"access_list\": [         {           \"name\": \"Automated\",           \"description\": \"\",           \"key\": \"ReceiveAlerts:Automated\"         },         {           \"name\": \"Manual\",           \"description\": \"\",           \"key\": \"ReceiveAlerts:Manual\"         }       ]     },     {       \"group_name\": \"Send Alerts\",       \"group_description\": \"Send Manual Alerts to selected user segment when required.\",       \"access_list\": [         {           \"name\": \"Send Manual Alerts\",           \"description\": \"\",           \"key\": \"SendAlerts:Manual\"         }       ]     },     {       \"group_name\": \"Configure Stations\",       \"group_description\": \"Configure Stations,e.g. Threshold Limit etc. of various assigned stations.\",       \"access_list\": [         {           \"name\": \"Set Thresholds for Alert Generation\",           \"description\": \"\",           \"key\": \"ConfigureStations:SetAlertThresholds\"         }       ]     },     {       \"group_name\": \"Configure Alerts\",       \"group_description\": \"Configure when to generate an alert and to who it should be delivered\",       \"access_list\": [         {           \"name\": \"Automated\",           \"description\": \"\",           \"key\": \"ConfigureAlerts:Automated\"         },         {           \"name\": \"Manual\",           \"description\": \"\",           \"key\": \"ConfigureAlerts:Manual\"         }       ]     },     {       \"group_name\": \"Manage Users\",       \"group_description\": \"Manage users and their access to various features\",       \"access_list\": [         {           \"name\": \"Get List of Existing Users\",           \"description\": \"\",           \"key\": \"ManageUsers:GetUser\"         },         {           \"name\": \"Create New User\",           \"description\": \"\",           \"key\": \"ManageUsers:CreateUser\"         },         {           \"name\": \"Modify Existing User\",           \"description\": \"\",           \"key\": \"ManageUsers:EditUser\"         },         {           \"name\": \"Delete Existing User\",           \"description\": \"\",           \"key\": \"ManageUsers:DeleteUser\"         },         {           \"name\": \"Get List of User Groups\",           \"description\": \"\",           \"key\": \"ManageUsers:GetUserGroup\"         },         {           \"name\": \"Create New User Group\",           \"description\": \"\",           \"key\": \"ManageUsers:CreateUserGroup\"         },         {           \"name\": \"Modify Existing User Group\",           \"description\": \"\",           \"key\": \"ManageUsers:EditUserGroup\"         },         {           \"name\": \"Delete Existing User Group\",           \"description\": \"\",           \"key\": \"ManageUsers:DeleteUserGroup\"         },         {           \"name\": \"Get List of Existing User Roles\",           \"description\": \"\",           \"key\": \"ManageUsers:GetUserRole\"         },         {           \"name\": \"Create New User Role\",           \"description\": \"\",           \"key\": \"ManageUsers:CreateUserRole\"         },         {           \"name\": \"Modify Existing User Role\",           \"description\": \"\",           \"key\": \"ManageUsers:EditUserRole\"         },         {           \"name\": \"Delete Existing User Role\",           \"description\": \"\",           \"key\": \"ManageUsers:DeleteUserRole\"         }       ]     }   ] }');
INSERT INTO `services_tbl` VALUES(19, 'Flood Monitoring', 'flood-monitoring', 'v0.1.0', '{\r\n  \"version\": \"1\",\r\n  \"access_groups\": [\r\n    {\r\n      \"group_name\": \"Access Data\",\r\n      \"group_description\": \"Access Data of various stations\",\r\n      \"access_list\": [\r\n        {\r\n          \"name\": \"Real-time Data\",\r\n          \"description\": \"Access Real-time Data of various stations.\",\r\n          \"key\": \"AccessData:RealTime\"\r\n        },\r\n        {\r\n          \"name\": \"Historical Data\",\r\n          \"description\": \"\",\r\n          \"key\": \"AccessData:HistoricalData\"\r\n        }\r\n      ]\r\n    },\r\n    {\r\n      \"group_name\": \"Advanced Analytics\",\r\n      \"group_description\": \"Access Advanced Analytics of various stations.\",\r\n      \"access_list\": [\r\n        {\r\n          \"name\": \"Forecasting\",\r\n          \"description\": \"\",\r\n          \"key\": \"AdvancedAnalytics:Forecasting\"\r\n        },\r\n        {\r\n          \"name\": \"Scenarios\",\r\n          \"description\": \"\",\r\n          \"key\": \"AdvancedAnalytics:Scenarios\"\r\n        }\r\n      ]\r\n    },\r\n    {\r\n      \"group_name\": \"Receive Alerts\",\r\n      \"group_description\": \"Receive Alerts for assigned events.\",\r\n      \"access_list\": [\r\n        {\r\n          \"name\": \"Automated\",\r\n          \"description\": \"\",\r\n          \"key\": \"ReceiveAlerts:Automated\"\r\n        },\r\n        {\r\n          \"name\": \"Manual\",\r\n          \"description\": \"\",\r\n          \"key\": \"ReceiveAlerts:Manual\"\r\n        }\r\n      ]\r\n    },\r\n    {\r\n      \"group_name\": \"Send Alerts\",\r\n      \"group_description\": \"Send Manual Alerts to selected user segment when required.\",\r\n      \"access_list\": [\r\n        {\r\n          \"name\": \"Send Manual Alerts\",\r\n          \"description\": \"\",\r\n          \"key\": \"SendAlerts:Manual\"\r\n        }\r\n      ]\r\n    },\r\n    {\r\n      \"group_name\": \"Configure Stations\",\r\n      \"group_description\": \"Configure Stations,e.g. Threshold Limit etc. of various assigned stations.\",\r\n      \"access_list\": [\r\n        {\r\n          \"name\": \"Set Thresholds for Alert Generation\",\r\n          \"description\": \"\",\r\n          \"key\": \"ConfigureStations:SetAlertThresholds\"\r\n        }\r\n      ]\r\n    },\r\n    {\r\n      \"group_name\": \"Configure Alerts\",\r\n      \"group_description\": \"Configure when to generate an alert and to who it should be delivered\",\r\n      \"access_list\": [\r\n        {\r\n          \"name\": \"Automated\",\r\n          \"description\": \"\",\r\n          \"key\": \"ConfigureAlerts:Automated\"\r\n        },\r\n        {\r\n          \"name\": \"Manual\",\r\n          \"description\": \"\",\r\n          \"key\": \"ConfigureAlerts:Manual\"\r\n        }\r\n      ]\r\n    },\r\n    {\r\n      \"group_name\": \"Manage Users\",\r\n      \"group_description\": \"Manage users and their access to various features\",\r\n      \"access_list\": [\r\n        {\r\n          \"name\": \"Get List of Existing Users\",\r\n          \"description\": \"\",\r\n          \"key\": \"ManageUsers:GetUser\"\r\n        },\r\n        {\r\n          \"name\": \"Create New User\",\r\n          \"description\": \"\",\r\n          \"key\": \"ManageUsers:CreateUser\"\r\n        },\r\n        {\r\n          \"name\": \"Modify Existing User\",\r\n          \"description\": \"\",\r\n          \"key\": \"ManageUsers:EditUser\"\r\n        },\r\n        {\r\n          \"name\": \"Delete Existing User\",\r\n          \"description\": \"\",\r\n          \"key\": \"ManageUsers:DeleteUser\"\r\n        },\r\n        {\r\n          \"name\": \"Get List of User Groups\",\r\n          \"description\": \"\",\r\n          \"key\": \"ManageUsers:GetUserGroup\"\r\n        },\r\n        {\r\n          \"name\": \"Create New User Group\",\r\n          \"description\": \"\",\r\n          \"key\": \"ManageUsers:CreateUserGroup\"\r\n        },\r\n        {\r\n          \"name\": \"Modify Existing User Group\",\r\n          \"description\": \"\",\r\n          \"key\": \"ManageUsers:EditUserGroup\"\r\n        },\r\n        {\r\n          \"name\": \"Delete Existing User Group\",\r\n          \"description\": \"\",\r\n          \"key\": \"ManageUsers:DeleteUserGroup\"\r\n        },\r\n        {\r\n          \"name\": \"Get List of Existing User Roles\",\r\n          \"description\": \"\",\r\n          \"key\": \"ManageUsers:GetUserRole\"\r\n        },\r\n        {\r\n          \"name\": \"Create New User Role\",\r\n          \"description\": \"\",\r\n          \"key\": \"ManageUsers:CreateUserRole\"\r\n        },\r\n        {\r\n          \"name\": \"Modify Existing User Role\",\r\n          \"description\": \"\",\r\n          \"key\": \"ManageUsers:EditUserRole\"\r\n        },\r\n        {\r\n          \"name\": \"Delete Existing User Role\",\r\n          \"description\": \"\",\r\n          \"key\": \"ManageUsers:DeleteUserRole\"\r\n        }\r\n      ]\r\n    }\r\n  ]\r\n}');

CREATE TABLE `station_groups` (
  `stgr_id` bigint(20) UNSIGNED NOT NULL,
  `serv_id` bigint(20) UNSIGNED NOT NULL,
  `clep_id` bigint(20) UNSIGNED NOT NULL,
  `stgr_name` varchar(100) NOT NULL,
  `stgr_description` text NOT NULL,
  `stgr_created_by` bigint(20) UNSIGNED NOT NULL,
  `stgr_created_at` bigint(10) UNSIGNED NOT NULL,
  `stgr_created_from` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `station_groups` VALUES(1, 18, 2, 'Station Group1', '', 2, 1537421966, '');
INSERT INTO `station_groups` VALUES(2, 18, 2, 'Station Group 2', '', 3, 1537421966, '');

CREATE TABLE `station_group_links` (
  `stgr_id` bigint(20) UNSIGNED NOT NULL,
  `stations_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `station_group_links` VALUES(2, 3);
INSERT INTO `station_group_links` VALUES(2, 5);
INSERT INTO `station_group_links` VALUES(2, 4);
INSERT INTO `station_group_links` VALUES(1, 1);

CREATE TABLE `user_groups` (
  `ug_id` bigint(20) UNSIGNED NOT NULL,
  `serv_id` bigint(20) UNSIGNED NOT NULL,
  `clep_id` bigint(20) UNSIGNED NOT NULL,
  `ug_name` varchar(255) NOT NULL,
  `ug_description` varchar(255) NOT NULL,
  `ur_id` bigint(20) UNSIGNED NOT NULL,
  `ug_created_by` bigint(20) UNSIGNED NOT NULL,
  `ug_created_at` bigint(10) UNSIGNED NOT NULL,
  `ug_created_from` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user_groups` VALUES(1, 18, 2, 'North Managers', 'Managers for north wing stations', 2, 1, 1537249944, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36');
INSERT INTO `user_groups` VALUES(3, 18, 2, 'North Managers', 'Managers for north wing stations', 2, 1, 1537266596, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36');

CREATE TABLE `user_groups_station_link` (
  `del_me` int(11) NOT NULL,
  `ug_id` bigint(20) UNSIGNED NOT NULL,
  `ugsl_link_id` bigint(20) UNSIGNED NOT NULL,
  `ugsl_what_link` tinyint(2) UNSIGNED NOT NULL COMMENT '1 -> station id, 2-> station group id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user_groups_station_link` VALUES(3, 1, 1, 2);
INSERT INTO `user_groups_station_link` VALUES(5, 1, 2, 2);

CREATE TABLE `user_groups_user_link` (
  `ug_id` bigint(20) UNSIGNED NOT NULL,
  `usr_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user_groups_user_link` VALUES(3, 1);
INSERT INTO `user_groups_user_link` VALUES(3, 2);
INSERT INTO `user_groups_user_link` VALUES(3, 3);
INSERT INTO `user_groups_user_link` VALUES(1, 3);
INSERT INTO `user_groups_user_link` VALUES(1, 4);

CREATE TABLE `user_notification_tokens` (
  `usr_id` bigint(20) UNSIGNED NOT NULL,
  `unt_token` varchar(255) NOT NULL,
  `unt_type` tinyint(1) UNSIGNED NOT NULL COMMENT '1 -> FCM id',
  `unt_source_os` tinyint(1) UNSIGNED NOT NULL COMMENT '1 -> Android',
  `unt_is_active` tinyint(1) UNSIGNED NOT NULL COMMENT '1 -> Active, 0 -> In active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user_notification_tokens` VALUES(1, 'fSuFsD2zX4', 1, 1, 0);
INSERT INTO `user_notification_tokens` VALUES(3, 'fSuFsD2zX4', 1, 1, 1);

CREATE TABLE `user_roles` (
  `ur_id` bigint(20) UNSIGNED NOT NULL,
  `clep_id` bigint(20) UNSIGNED NOT NULL,
  `serv_id` bigint(20) UNSIGNED NOT NULL,
  `ur_name` varchar(255) NOT NULL,
  `ur_description` varchar(255) NOT NULL,
  `ur_access` text NOT NULL,
  `ur_created_at` bigint(10) UNSIGNED NOT NULL,
  `ur_created_by` bigint(20) UNSIGNED NOT NULL,
  `ur_created_from` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `user_roles` VALUES(2, 2, 18, 'Manager', 'I am a manager', '{\r\n\r\n\"ConfigureAlerts:Automated\":true,\r\n\"AccessData:HistoricalData\":true,\r\n\"AccessData:RealTime\":true\r\n}', 1537184584, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36');
INSERT INTO `user_roles` VALUES(3, 2, 18, 'Worker', 'I am a worker', '{\"master_access\":\"master_access\"}', 1537184606, 1, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36');

CREATE TABLE `iot_issue_tracking_action_logs` (
  `iit_id` bigint(20) UNSIGNED NOT NULL,
  `iital_type` tinyint(1) NOT NULL COMMENT '0 -> Alert, 1 -> User Action, 2 -> Weightage Change',
  `iital_alert_sent_to` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `iitl_action_time` bigint(20) UNSIGNED NOT NULL,
  `iitl_action_details` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `iitl_action_taken_by` bigint(20) UNSIGNED NOT NULL,
  `iitl_action_taken_from_ua` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `iot_issue_tracking_tickets` (
  `iitt_id` bigint(20) UNSIGNED NOT NULL,
  `clep_id` bigint(20) UNSIGNED NOT NULL,
  `serv_id` bigint(20) UNSIGNED NOT NULL,
  `iitt_station_id` bigint(20) UNSIGNED NOT NULL,
  `srvc_id` bigint(20) UNSIGNED NOT NULL,
  `iitr_id` bigint(20) UNSIGNED NOT NULL,
  `rule_id` bigint(20) UNSIGNED NOT NULL,
  `iitt_status` tinyint(1) NOT NULL COMMENT '0 -> Unattended, 1 ->  Attended, 2 -> Resolved',
  `iitt_title` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `iitt_ticket_data` text NOT NULL,
  `iitt_created_at` bigint(10) NOT NULL,
  `iitt_resolved_at` bigint(10) UNSIGNED NOT NULL,
  `iitt_last_attended_at` bigint(10) NOT NULL,
  `iitt_weightage` tinyint(1) NOT NULL COMMENT '0-> New, 1-> High, 2-> Critical'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `iot_issue_tracking_tickets` VALUES(3, 2, 18, 1, 0, 0, 9, 0, '', '{\r\n  \"max_temp\": 20,\r\n  \"threshold\": {\r\n    \"min\": -5,\r\n    \"max\": 5\r\n  }\r\n}', 1539260889, 1539261489, 1539261489, 1);
INSERT INTO `iot_issue_tracking_tickets` VALUES(4, 2, 18, 1, 0, 0, 9, 0, '', '{\r\n  \"max_temp\": 10,\r\n  \"threshold\": {\r\n    \"min\": -5,\r\n    \"max\": 5\r\n  }\r\n}', 1539270889, 1539271489, 1539271489, 1);
INSERT INTO `iot_issue_tracking_tickets` VALUES(5, 2, 18, 1, 0, 0, 10, 0, '', '', 1539280889, 1539281489, 1539281489, 1);
INSERT INTO `iot_issue_tracking_tickets` VALUES(6, 2, 18, 1, 0, 0, 9, 0, '', '{\r\n  \"max_temp\": 10,\r\n  \"threshold\": {\r\n    \"min\": -5,\r\n    \"max\": 5\r\n  }\r\n}', 1540021191, 1540022191, 1540022191, 1);
INSERT INTO `iot_issue_tracking_tickets` VALUES(7, 2, 18, 1, 0, 0, 9, 0, '', '{\r\n  \"max_temp\": 10,\r\n  \"threshold\": {\r\n    \"min\": -5,\r\n    \"max\": 5\r\n  }\r\n}', 1540023191, 1540023191, 1540023191, 1);
INSERT INTO `iot_issue_tracking_tickets` VALUES(8, 2, 18, 1, 0, 0, 10, 0, '', '', 1540024200, 1540024300, 1540024300, 1);

ALTER TABLE `alert_rules`
  ADD PRIMARY KEY (`rule_id`);

ALTER TABLE `automated_rule_template`
  ADD PRIMARY KEY (`art_id`);

ALTER TABLE `client_end_point_tbl`
  ADD PRIMARY KEY (`clep_id`),
  ADD KEY `area_id` (`area_id`);

ALTER TABLE `cold_storage_monitoring_stations`
  ADD PRIMARY KEY (`dstn_id`);

ALTER TABLE `datoms_alert_templates`
  ADD PRIMARY KEY (`dat_id`);

ALTER TABLE `datoms_stations`
  ADD PRIMARY KEY (`dstn_id`);

ALTER TABLE `events_table`
  ADD PRIMARY KEY (`event_id`);

ALTER TABLE `iot_devices`
  ADD PRIMARY KEY (`idev_id`);

ALTER TABLE `iot_device_types`
  ADD PRIMARY KEY (`idt_id`);

ALTER TABLE `manual_rule_template`
  ADD PRIMARY KEY (`mat_id`);

ALTER TABLE `services_tbl`
  ADD PRIMARY KEY (`serv_id`);

ALTER TABLE `station_groups`
  ADD PRIMARY KEY (`stgr_id`);

ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`ug_id`);

ALTER TABLE `user_groups_station_link`
  ADD PRIMARY KEY (`del_me`);

ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`ur_id`);

ALTER TABLE `iot_issue_tracking_tickets`
  ADD PRIMARY KEY (`iitt_id`);


ALTER TABLE `alert_rules`
  MODIFY `rule_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
ALTER TABLE `automated_rule_template`
  MODIFY `art_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
ALTER TABLE `client_end_point_tbl`
  MODIFY `clep_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=366;
ALTER TABLE `cold_storage_monitoring_stations`
  MODIFY `dstn_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
ALTER TABLE `datoms_alert_templates`
  MODIFY `dat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
ALTER TABLE `datoms_stations`
  MODIFY `dstn_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
ALTER TABLE `events_table`
  MODIFY `event_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6601;
ALTER TABLE `iot_devices`
  MODIFY `idev_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
ALTER TABLE `manual_rule_template`
  MODIFY `mat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
ALTER TABLE `services_tbl`
  MODIFY `serv_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
ALTER TABLE `station_groups`
  MODIFY `stgr_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
ALTER TABLE `user_groups`
  MODIFY `ug_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
ALTER TABLE `user_groups_station_link`
  MODIFY `del_me` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
ALTER TABLE `user_roles`
  MODIFY `ur_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
ALTER TABLE `iot_issue_tracking_tickets`
  MODIFY `iitt_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

COMMIT;
